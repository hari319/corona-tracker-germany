/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Context, CountryData } from '../Context';
import TableView from './TableView';
import ImageUrlJSON from '../../JSON/ImageUrl.json';

const TableSelection = () => {
  const {
    selectedRegion,
    weeks,
    regionData,
    collapseRegionData,
    radioValue,
    showTableChart,
  } = useContext(Context);
  const { t } = useTranslation();
  const [rowsState, setRowsState] = useState<CountryData[]>([]);

  useEffect(() => {
    setRowsState([]);
    regionData &&
      regionData.forEach((rowData) => {
        setRowsState((row) => [...row, rowData]);
      });
  }, [regionData]);

  const checkCondition = () => {
    switch (true) {
      case selectedRegion.code === 'all' && weeks === 0:
        return true;
      case selectedRegion.code !== 'all' && [7, 14, 21, 28].includes(weeks):
        return true;
      default:
        return false;
    }
  };

  const getCollapseTableData = (name: string) => {
    if (selectedRegion.code === 'all') {
      const tableData: any = collapseRegionData.find((obj) =>
        obj.hasOwnProperty(name)
      );
      return tableData[name];
    } else {
      return collapseRegionData;
    }
  };

  const getColumns = (regionType: string) => {
    let title: string = '';
    switch (true) {
      case selectedRegion.code === 'all' && regionType === 'states':
        title = t('STATES');
        break;
      case selectedRegion.code === 'all' && regionType === 'districts':
        title = t('DISTRICTS');
        break;
      case radioValue === 'states':
        title = t('STATE');
        break;
      case radioValue === 'districts':
        title = t('DISTRICT');
        break;
    }

    const getImage = (name: string) => {
      const ImageUrl: { [key: string]: string } = ImageUrlJSON;
      return <img alt={name} src={ImageUrl[name]} height="30px" />;
    };

    return [
      {
        field: 'name',
        title: title,
        render: (rowData: CountryData) => {
          return (
            <>
              {getImage(rowData.name)}
              <br />
              {rowData.name}
            </>
          );
        },
        cellStyle: {
          textAlign: 'center',
        },
      },
      {
        field: 'date',
        title: t('DATE'),
        cellStyle: {
          textAlign: 'center',
        },
      },
      {
        field: 'cases',
        title: t('CASES'),
        cellStyle: {
          textAlign: 'center',
        },
      },
      {
        field: 'recovered',
        title: t('RECOVERED'),
        cellStyle: {
          textAlign: 'center',
        },
      },
      {
        field: 'deaths',
        title: t('DEATHS'),
        cellStyle: {
          textAlign: 'center',
        },
      },
    ];
  };

  return showTableChart ? (
    checkCondition() ? (
      <TableView
        columns={getColumns('states')}
        data={rowsState}
        title={`${t('TABLETAG')} ${
          selectedRegion.code === 'all' ? t('GERMANY') : selectedRegion.name
        }`}
        detailPanel={(rowData: CountryData) => {
          return (
            <div style={{ padding: '35px' }}>
              <TableView
                columns={getColumns('districts')}
                data={getCollapseTableData(rowData.name)}
                title={`${
                  selectedRegion.code === 'all'
                    ? t('TABLETAG') + t('GERMANYDISTRICT')
                    : ''
                }`}
              />
            </div>
          );
        }}
        onRowClick={(event: any, rowData: any, togglePanel: any) =>
          togglePanel()
        }
      />
    ) : (
      <TableView
        columns={getColumns('states')}
        data={rowsState}
        title={`${t('TABLETAG')} ${
          selectedRegion.code === 'all' ? t('GERMANY') : selectedRegion.name
        }`}
      />
    )
  ) : (
    <></>
  );
};

export default TableSelection;
