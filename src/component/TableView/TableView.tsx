import React, {
  forwardRef,
  useState,
  useEffect,
  useContext,
  useRef,
} from 'react';
import MaterialTable from 'material-table';
import {
  AddBox,
  ArrowDownward,
  Check,
  ChevronLeft,
  ChevronRight,
  Clear,
  DeleteOutline,
  Edit,
  FilterList,
  FirstPage,
  LastPage,
  Remove,
  SaveAlt,
  Search,
  ViewColumn,
} from '@material-ui/icons';
import { Paper } from '@material-ui/core';
import { SvgIconProps } from '@material-ui/core/SvgIcon';
import { Context, CountryData } from '../Context';
import Chart from '../Chart/Chart';
import styles from './TableView.module.css';

const tableIcons: any = {
  Add: forwardRef<unknown, SvgIconProps>((props, ref: any) => (
    <AddBox {...props} ref={ref} />
  )),
  Check: forwardRef<unknown, SvgIconProps>((props, ref: any) => (
    <Check {...props} ref={ref} />
  )),
  Clear: forwardRef<unknown, SvgIconProps>((props, ref: any) => (
    <Clear {...props} ref={ref} />
  )),
  Delete: forwardRef<unknown, SvgIconProps>((props, ref: any) => (
    <DeleteOutline {...props} ref={ref} />
  )),
  DetailPanel: forwardRef<unknown, SvgIconProps>((props, ref: any) => (
    <ChevronRight {...props} ref={ref} />
  )),
  Edit: forwardRef<unknown, SvgIconProps>((props, ref: any) => (
    <Edit {...props} ref={ref} />
  )),
  Export: forwardRef<unknown, SvgIconProps>((props, ref: any) => (
    <SaveAlt {...props} ref={ref} />
  )),
  Filter: forwardRef<unknown, SvgIconProps>((props, ref: any) => (
    <FilterList {...props} ref={ref} />
  )),
  FirstPage: forwardRef<unknown, SvgIconProps>((props, ref: any) => (
    <FirstPage {...props} ref={ref} />
  )),
  LastPage: forwardRef<unknown, SvgIconProps>((props, ref: any) => (
    <LastPage {...props} ref={ref} />
  )),
  NextPage: forwardRef<unknown, SvgIconProps>((props, ref: any) => (
    <ChevronRight {...props} ref={ref} />
  )),
  PreviousPage: forwardRef<unknown, SvgIconProps>((props, ref: any) => (
    <ChevronLeft {...props} ref={ref} />
  )),
  ResetSearch: forwardRef<unknown, SvgIconProps>((props, ref: any) => (
    <Clear {...props} ref={ref} />
  )),
  Search: forwardRef<unknown, SvgIconProps>((props, ref: any) => (
    <Search {...props} ref={ref} />
  )),
  SortArrow: forwardRef<unknown, SvgIconProps>((props, ref: any) => (
    <ArrowDownward {...props} ref={ref} />
  )),
  ThirdStateCheck: forwardRef<unknown, SvgIconProps>((props, ref: any) => (
    <Remove {...props} ref={ref} />
  )),
  ViewColumn: forwardRef<unknown, SvgIconProps>((props, ref: any) => (
    <ViewColumn {...props} ref={ref} />
  )),
};

const TableViewMaterial = ({ ...props }) => {
  const tableRef = useRef();
  const [chartDatas, setChartData] = useState<CountryData[]>([]);
  const { selectedRegion, radioValue, weeks } = useContext(Context);

  const updateChartData = () => {
    let tableCurrentData: any = tableRef?.current;
    setChartData(tableCurrentData.dataManager.pagedData);
  };

  const getChartType = () => {
    switch (true) {
      case props.columns[1].title === 'States' ||
        props.columns[1].title === 'Bundeslander':
        return 'states';
      case selectedRegion.code === 'all' && weeks === 0:
        return 'districts';
      default:
        return radioValue;
    }
  };

  useEffect(() => {
    updateChartData();
  }, [props.data]);

  return (
    <>
      <div id={props.id}>
        <Paper variant="outlined" className={styles.tableViewPaper}>
          <MaterialTable
            tableRef={tableRef}
            icons={tableIcons}
            columns={props.columns}
            data={props.data}
            options={{
              headerStyle: {
                backgroundColor: 'greenyellow',
                fontWeight: 'bold',
                textAlign: 'center',
              },
            }}
            onChangePage={() => {
              updateChartData();
            }}
            onOrderChange={() => {
              updateChartData();
            }}
            onSearchChange={() => {
              updateChartData();
            }}
            onChangeRowsPerPage={() => {
              updateChartData();
            }}
            {...props}
          />
        </Paper>
      </div>
      <Chart chartDatas={chartDatas} viewType={getChartType()} />
    </>
  );
};

export default TableViewMaterial;
