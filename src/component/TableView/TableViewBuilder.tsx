import React, { useState, useContext, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { makeStyles } from '@material-ui/core/styles';
import {
  Grid,
  Select,
  MenuItem,
  InputLabel,
  FormControl,
  Button,
  useMediaQuery,
  useTheme,
} from '@material-ui/core';
import {
  Visibility as VisibilityIcon,
  VisibilityOff as VisibilityOffIcon,
} from '@material-ui/icons';
import { Context, SelectedCountryValue } from '../Context';
import GermanyStates from '../../JSON/GermanyStates.json';
import GermnayDistricts from '../../JSON/GermnayDistricts.json';
import {
  fetchDataALL,
  fetchDataPerWeek,
  fetchDataAllDistricts,
  fetchDataAllState,
} from '../../api/api';

const TableViewBuilder = () => {
  const { t } = useTranslation();
  const theme = useTheme();
  const matchesMobile = useMediaQuery(theme.breakpoints.down('xs'));
  const {
    setSelectedRegion,
    setWeeks,
    setRegionData,
    setCollapseRegionData,
    setShowTableChart,
    setLoadSpinner,
    radioValue,
  } = useContext(Context);
  const [countryPicker, setCountryPicker] = useState<SelectedCountryValue>({
    name: '',
    code: '',
    group: '',
  });
  const [countryPickerInput, setCountryPickerInput] = useState<string>();
  const [WeeksPicker, setWeeksPicker] = useState<number>(1);
  let menuDisable: boolean =
    countryPicker && countryPicker.code === 'all' ? true : false;
  const btnDisable: boolean =
    countryPicker.name === '' || WeeksPicker === undefined;
  const weeksObj: { value: number; label: string }[] = [
    {
      value: 1,
      label: t('LATEST'),
    },
    {
      value: 7,
      label: '1 ' + t('WEEK'),
    },
    {
      value: 14,
      label: '2 ' + t('WEEKS'),
    },
    {
      value: 21,
      label: '3 ' + t('WEEKS'),
    },
    {
      value: 28,
      label: '4 ' + t('WEEKS'),
    },
    {
      value: 0,
      label: t('ALL'),
    },
  ];

  const useStyles = makeStyles(() => ({
    row: {
      display: matchesMobile ? 'grid' : 'flex',
      flexDirection: matchesMobile ? 'column' : 'row',
      alignItems: 'center',
    },
    mobileView: {
      display: matchesMobile ? 'contents' : '',
    },
    option: {
      fontSize: 15,
      '& > span': {
        marginRight: 10,
        fontSize: 18,
      },
    },
    formControl: {
      marginTop: matchesMobile ? '15px' : '',
      minWidth: 300,
    },
    button: {
      minWidth: 300,
      marginTop: matchesMobile ? '15px' : '',
      height: 55,
    },
  }));

  const classes = useStyles();

  useEffect(() => {
    setCountryPicker({ name: '', code: '', group: '' });
    setCountryPickerInput('');
    setWeeksPicker(1);
  }, [radioValue]);

  const handleOnClick = async () => {
    let countryData: any = '';
    let collapseTableData: any = '';
    setLoadSpinner(true);

    switch (true) {
      case countryPicker.code === 'all':
        countryData = await fetchDataAllState();
        collapseTableData = await fetchDataAllDistricts();
        setRegionData(countryData);
        setCollapseRegionData(collapseTableData);
        break;
      case WeeksPicker === 0:
        countryData = await fetchDataALL(radioValue, countryPicker.code);
        setRegionData(countryData);
        break;
      case WeeksPicker !== 0:
        countryData = await fetchDataPerWeek(
          radioValue,
          countryPicker.code,
          WeeksPicker
        );
        if (WeeksPicker === 1) {
          countryData
            ? setRegionData(countryData.single)
            : setLoadSpinner(false);
        } else {
          setRegionData(countryData.total);
          setCollapseRegionData(countryData.single);
        }
        break;
    }

    setWeeks(WeeksPicker);
    setSelectedRegion(countryPicker);
    setLoadSpinner(false);
    setShowTableChart(true);
  };

  return radioValue ? (
    <div className={classes.row}>
      <Grid
        container
        direction={matchesMobile ? 'column' : 'row'}
        justify="center"
        alignItems="center"
        wrap="nowrap"
        spacing={3}
      >
        <Grid container item xs={4} className={classes.mobileView}>
          <Autocomplete
            value={countryPicker}
            inputValue={countryPickerInput}
            id="country-picker"
            style={{ width: 300 }}
            options={
              radioValue === 'states'
                ? (GermanyStates as SelectedCountryValue[])
                : (GermnayDistricts as SelectedCountryValue[])
            }
            classes={{
              option: classes.option,
            }}
            groupBy={(option) => option.group}
            autoHighlight
            getOptionLabel={(option) => option.name}
            renderOption={(option) => (
              <React.Fragment>
                <span>{option.name}</span>
              </React.Fragment>
            )}
            onChange={(e, value) => {
              switch (true) {
                case value === null:
                  setCountryPicker({ name: '', code: '', group: '' });
                  break;
                case value?.code === 'all':
                  value && setCountryPicker(value);
                  setWeeksPicker(0);
                  break;
                case value?.code !== 'all':
                  value && setCountryPicker(value);
                  setWeeksPicker(1);
                  break;
                default:
                  value && setCountryPicker(value);
              }
            }}
            onInputChange={(event, value) => {
              setCountryPickerInput(value);
            }}
            renderInput={(params) => (
              <TextField
                {...params}
                label={
                  radioValue === 'states'
                    ? t('STATECHOSSE')
                    : t('DISTRICTCHOSSE')
                }
                variant="outlined"
                inputProps={{
                  ...params.inputProps,
                  autoComplete: 'new-password', // disable autocomplete and autofill
                }}
              />
            )}
          />
        </Grid>
        <Grid container item xs={4} className={classes.mobileView}>
          <FormControl variant="outlined" className={classes.formControl}>
            <InputLabel id="select-label">{t('WEEKSSELECT')}</InputLabel>
            <Select
              labelId="select"
              id="select"
              value={WeeksPicker}
              onChange={(event: any) => {
                let value: number = event.target.value;
                setWeeksPicker(value);
              }}
              label={t('WEEKSSELECT')}
            >
              {weeksObj.map((e, i) => {
                return i === weeksObj.length - 1 ? (
                  <MenuItem value={e.value}> {e.label}</MenuItem>
                ) : (
                  <MenuItem value={e.value} disabled={menuDisable}>
                    {e.label}
                  </MenuItem>
                );
              })}
            </Select>
          </FormControl>
        </Grid>
        <Grid container item xs={3} className={classes.mobileView}>
          <Button
            className={classes.button}
            variant="contained"
            color="primary"
            size="medium"
            disabled={btnDisable}
            onClick={handleOnClick}
            startIcon={btnDisable ? <VisibilityOffIcon /> : <VisibilityIcon />}
          >
            {t('SHOW')}
          </Button>
        </Grid>
      </Grid>
    </div>
  ) : (
    <></>
  );
};

export default TableViewBuilder;
