/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { createContext, useState, FunctionComponent } from 'react';

export interface SelectedCountryValue {
  name: string;
  code: string;
  group: string;
}
export interface CountryData {
  cases: number;
  recovered: number;
  deaths: number;
  date: Date;
  name: string;
}

export interface GermanyData {
  type: string;
  number: number;
  difference: number;
  lastUpdate: Date;
}

export const Context = createContext<{
  setSelectedRegion: (value: SelectedCountryValue) => void;
  setWeeks: (value: number) => void;
  setGermanyData: (value: GermanyData[]) => void;
  setRegionData: (value: CountryData[]) => void;
  setCollapseRegionData: (value: CountryData[]) => void;
  setRadioValue: (value: string) => void;
  setShowTableChart: (value: boolean) => void;
  setLoadSpinner: (value: boolean) => void;
  selectedRegion: SelectedCountryValue;
  weeks: number;
  regionData: CountryData[];
  collapseRegionData: CountryData[];
  germanyData: GermanyData[];
  radioValue: string;
  showTableChart: boolean;
  loadSpinner: boolean;
}>({
  setSelectedRegion: () => {},
  setGermanyData: () => {},
  setWeeks: () => {},
  setRegionData: () => {},
  setCollapseRegionData: () => {},
  setRadioValue: () => {},
  setShowTableChart: () => {},
  setLoadSpinner: () => {},
  selectedRegion: {
    name: '',
    code: '',
    group: '',
  },
  weeks: 1,
  germanyData: [],
  regionData: [],
  collapseRegionData: [],
  radioValue: '',
  showTableChart: false,
  loadSpinner: false,
});

const ContextProvider: FunctionComponent<any> = ({ children }) => {
  return <Context.Provider value={Auth()}>{children}</Context.Provider>;
};

const Auth = () => {
  const [germanyData, setGermanyData] = useState<GermanyData[]>([]);
  const [selectedRegion, setSelectedRegion] = useState<SelectedCountryValue>({
    name: '',
    code: '',
    group: '',
  });
  const [regionData, setRegionData] = useState<CountryData[]>([]);
  const [collapseRegionData, setCollapseRegionData] = useState<CountryData[]>(
    []
  );
  const [weeks, setWeeks] = useState<number>(1);
  const [radioValue, setRadioValue] = useState<string>('');
  const [showTableChart, setShowTableChart] = useState<boolean>(false);
  const [loadSpinner, setLoadSpinner] = useState<boolean>(false);

  return {
    selectedRegion,
    weeks,
    regionData,
    germanyData,
    collapseRegionData,
    radioValue,
    showTableChart,
    loadSpinner,
    setSelectedRegion,
    setGermanyData,
    setWeeks,
    setRegionData,
    setCollapseRegionData,
    setRadioValue,
    setShowTableChart,
    setLoadSpinner,
  };
};

export default ContextProvider;
