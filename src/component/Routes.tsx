import React from 'react';
import {
  Route,
  BrowserRouter as Router,
  Switch,
  Redirect,
} from 'react-router-dom';
import Dashboard from './Dashboard';
import NotFound from './Roles/NotFound';

const Routes = () => (
  <Router>
    <Switch>
      <Route exact path="/">
        <Redirect to="/corona-tracker-germany" />
      </Route>
      <Route exact path="/corona-tracker-germany">
        <Dashboard />
      </Route>
      <Route exact path="*">
        <NotFound />
      </Route>
    </Switch>
  </Router>
);
export default Routes;
