import React, { useContext } from 'react';
import { Bar } from 'react-chartjs-2';
import { useTranslation } from 'react-i18next';
import { Paper } from '@material-ui/core';
import { Context, CountryData } from '../Context';
import './Chart.css';

const Chart = (props: { chartDatas: CountryData[]; viewType: string }) => {
  const { chartDatas, viewType } = props;
  const { t } = useTranslation();
  const { selectedRegion, radioValue } = useContext(Context);
  let CasesList: number[] = [];
  let LabelList: any[] = [];
  let DeathsList: number[] = [];
  let RecoveredList: number[] = [];
  const showDataBar: number = chartDatas.length;

  function getLabel(chartData: CountryData) {
    if (selectedRegion.code !== 'all') {
      return chartData.date;
    } else {
      return chartData.name + ' ' + chartData.date;
    }
  }

  chartDatas.length > 0 &&
    chartDatas.forEach((chartData: CountryData) => {
      CasesList.push(chartData.cases);
      LabelList.push(getLabel(chartData));
      DeathsList.push(chartData.deaths);
      RecoveredList.push(chartData.recovered);
    });

  const BarData = {
    labels: LabelList.slice(-showDataBar),
    type: 'polarArea',
    datasets: [
      {
        data: CasesList.slice(-showDataBar),
        backgroundColor: '#ff3333',
        label: t('CASES'),
      },

      {
        data: RecoveredList.slice(-showDataBar),
        backgroundColor: '#008000',
        label: t('RECOVERED'),
      },

      {
        data: DeathsList.slice(-showDataBar),
        backgroundColor: '#000000',
        label: t('DEATHS'),
      },
    ],
  };

  const chartOptions = {
    responsive: true,
    datetainAspectRatio: false,
  };

  function getText() {
    switch (true) {
      case selectedRegion.code === 'all' && viewType === 'states':
        return t('GERMANYSTATE');
      case radioValue === 'states' && viewType === 'districts':
        return t('GERMANYDISTRICT');
      default:
        return selectedRegion.name;
    }
  }

  return chartDatas.length > 0 ? (
    <Paper className={'Chart'}>
      <h1 className={'h1'}>
        {t('CHARTTAG')}
        {getText()}
      </h1>
      <Bar data={BarData} options={chartOptions}></Bar>
    </Paper>
  ) : (
    <></>
  );
};

export default Chart;
