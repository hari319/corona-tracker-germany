/* eslint-disable no-throw-literal */
import _ from "lodash"
import GermanyDistricts from "../JSON/GermnayDistricts.json"

const URL = "https://api.corona-zahlen.org";
const dateOptions = {
  year: 'numeric',
  month: '2-digit',
  day: '2-digit',
};
const todayDate = new Date().toLocaleDateString("de-DE", dateOptions)

const apiCall = async (apiType) => {
  return await fetch(`${URL}/${apiType}`, {
    method: 'GET',
  }).then((response) => {
      if (response.ok) {
          return response.json();
      }
    })
    .catch((error) => {
      throw new Error('FETCH ERROR:', error);
    });
}

//This API produces a list of total cases, recovered cases, and deaths for Germany as a whole.
const fetchDataGermany = async() => {
  return await apiCall("germany");
}

//This API returns a list of all state with total cases, recovered and deaths.
const fetchDataAllState = async () => {
  const fetchData = await apiCall("states")
  let finalResponse = []

  if (fetchData) {
      for (const property in fetchData.data) {
        let res = fetchData.data[property]
        finalResponse.push({
          name: res.name,
          date:todayDate,
          cases: res.cases,
          recovered: res.recovered,
          deaths:res.deaths
        })
    }
    return _.sortBy(finalResponse, 
      [
        function (obj)
        { return obj.name; }
      ]);
  } else {
     AlertNotification();
  }  
}

//This API returns a list of all state districts' data .
const fetchDataAllDistricts = async () => {
  const fetchData = await apiCall("districts");
  let responseData = []

  if (emptyCheck(fetchData.data)) {
    for (const property in fetchData.data) {
      let res = fetchData.data[property]
      let response = {
        name: (GermanyDistricts.find(obj => obj.code === res.ags)).name,
        date:todayDate,
        cases: res.cases,
        recovered: res.recovered,
        deaths:res.deaths
      }

      if (responseData.length > 0 && responseData.some(obj => Object.keys(obj).includes(res.state))) {
        responseData.forEach((e, i) => {
          if (Object.keys(e).includes(res.state)) {            
            responseData[i][res.state].push(response)
          }
        })        
      } else {
        responseData.push({
          [res.state]: [response]
        },)
      }
    }
    
    const returnValue = []
    responseData.forEach((e, i)=> {
      returnValue.push( _.mapValues(responseData[i], items => 
      _.orderBy(items, ['name'], ['asc'])))
    })

    return returnValue
  } else {
     AlertNotification();
  }  
}

// This API returns the entire list of a single state/district.
const fetchDataALL= async(APItype, valueAbbreviation) => {  
  const fetchData = await fetch(`${URL}/${APItype}/${valueAbbreviation}`, {
    method: 'GET',
  }).then((response) => {
      if (response.ok) {
        return response.json();
      }
    })
    .catch((error) => {
      throw new Error('FETCH ERROR:', error);
    });
  
  if (fetchData && fetchData.data) {
    let data = fetchData.data[valueAbbreviation]
    return [{
        date: todayDate,
        cases: data.cases,
        recovered: data.recovered,
        deaths: data.deaths,
        name: data.name
      }]
  } else {
    AlertNotification();
  }
}

// This API gives data for a particular state/district per request weeks.
const fetchDataPerWeek = async(APItype, valueAbbreviation, weeks) => {
  
  const apiCall = async (apiType) => {
    return await fetch(`${URL}/${APItype}/${valueAbbreviation}/history/${apiType}/${weeks}`, {
      method: 'GET',
    }).then(async (response) => {
        if (response.ok) {
            return await response.json();
        }
      }).catch((error) => {
        throw new Error('FETCH ERROR:', error);
      });
  }
  
  let cases = await apiCall("cases");
  let recovered = await apiCall("recovered");  
  let deaths = await apiCall("deaths");  
  const singleDayEntry = [];

  if (emptyCheck(cases.data[valueAbbreviation]) &&
        emptyCheck(recovered.data[valueAbbreviation]) &&
        emptyCheck(deaths.data[valueAbbreviation])) {
    let name = APItype === "states" ?
      cases.data[valueAbbreviation].name :
      (GermanyDistricts.find(obj => obj.code === valueAbbreviation)).name
    
    let latestDate = todayDate;
    for (let index = cases.data[valueAbbreviation].history.length; index > 0; --index) {
      if (index === cases.data[valueAbbreviation].history.length)
        latestDate =
          new Date(cases.data[valueAbbreviation].history[index - 1].date).toLocaleDateString("de-DE", dateOptions)
      singleDayEntry.push({
        cases: cases.data[valueAbbreviation].history[index -1].cases,
        recovered: recovered.data[valueAbbreviation].history[index -1].recovered,
        deaths: deaths.data[valueAbbreviation].history[index- 1].deaths,
        date:
          new Date(cases.data[valueAbbreviation].history[index - 1].date).toLocaleDateString("de-DE", dateOptions),
        name:name,
      });
    }
    
    let date = new Date();
    const lastDate = new Date(date.setDate(date.getDate() - weeks)).toLocaleDateString("de-DE", dateOptions)

    let finalReturn = {
      total: [{
        cases: singleDayEntry.reduce((a, {cases}) => a + cases, 0),
        recovered: singleDayEntry.reduce((a, {recovered}) => a + recovered, 0),
        deaths: singleDayEntry.reduce((a, {deaths}) => a + deaths, 0),
        date: `${latestDate} - ${lastDate}` ,
        name:name,
      }],
      single:singleDayEntry
    }
    return finalReturn;
  } else {
     AlertNotification();
  }
}

const emptyCheck = (data) => {
  return data && Object.keys(data).length > 0
}

const AlertNotification = () => {
  alert("No Data was returned!. Please retry later");
}

export {
  fetchDataGermany,
  fetchDataALL,
  fetchDataPerWeek,
  fetchDataAllDistricts,
  fetchDataAllState,
}