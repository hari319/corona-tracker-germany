# COVID-19 statistics Germany

[Live Demo](https://hari319.gitlab.io/corona-tracker-germany/)

### Reference

- Data from [Robert Koch-Institut](https://www.rki.de/DE/Content/Infekt/SurvStat/survstat_node.html)
- API made by [Marlon Lueckert](https://github.com/marlon360/rki-covid-api)
- Collection of icons from [icons8](https://icons8.de/)

### Information

- Corona statistics about Germany, states, and districts are displayed in this project. It includes information on the overall number of cases, deaths, and recovered cases.

## Technology Used

- React, Typescript, Material UI, i18next

## Key Features of App :

- Real time data of total number of Corona cases, Recovered, and Deaths in Germany (States + Districts) are shown in this app.
- It displays statistics from all affected states as well as data from affected districts.
- Robert Koch's Restful API provides real data.

## Setup

### `yarn install`

Install all the dependencies listed within package.json in the local node_modules folder.

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
